using System;

public class HelloWorld {
    public static void Main (string[] args) {
        Console.WriteLine ("Hello Mono World");
    }
}
To compile, use csc:

    csc hello.cs
Note : csc compiler is not available on all platforms or in very old Mono versions, in such cases use mcs instead.

The compiler will create“ hello.exe”, which you can run using:

    mono hello.exe
The program should run and output:

    Hello Mono World
HTTPS connections
To make sure HTTPS connections work, run the following command to check whether you can connect to nuget.org:

    csharp - e 'new System.Net.WebClient ().DownloadString ("https://www.nuget.org")'
The program prints the website contents
if everything works or throws an exception
if it doesn’ t.

WinForms Hello World
The following program tests writing a System.Windows.Forms application.

using System;
using System.Windows.Forms;

public class HelloWorld : Form {
    static public void Main () {
        Application.Run (new HelloWorld ());
    }

    public HelloWorld () {
        Text = "Hello Mono World";
    }
}
To compile, use csc with the - r option to tell the compiler to pull in the WinForms libraries:

    csc hello.cs - r : System.Windows.Forms.dll
The compiler will create“ hello.exe”, which you can run using:

    mono hello.exe
NOTE : on macOS you’ ll have to wait around a minute the very first time you run this command.You also need to use mono32 since WinForms isn’ t supported on 64 bit yet.As of macOS 10.15 Catalina there’ s no 32 bit support anymore so WinForms doesn’ t work there at the moment.

ASP.NET Hello World
Create a text file with the name hello.aspx and the content:

    <
    %
    @ Page Language = "C#" % >
    <
    html >
    <
    head >
    <
    title > Sample Calendar</ title>
    <
    /
    head >
    <
    asp : calendar showtitle = "true"
runat = "server" >
    <
    /
    asp : calendar >
    Then run the xsp4 command from that directory:

    xsp4--port 9000
Use a web browser to contact http: //localhost:9000/hello.aspx

    Gtk# Hello World
The following program tests writing a Gtk# application.

using System;
using Gtk;

class Hello {
    static void Main () {
        Application.Init ();

        Window window = new Window ("Hello Mono World");
        window.Show ();

        Application.Run ();
    }
}